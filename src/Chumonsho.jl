module Chumonsho

using DataStructures
using Dates
using CSV

"The side of a transaction"
@enum Side sell buy

"Get the side associated with a given flags string"
function parse_dxfeed_side(flags::AbstractString)::Union{Nothing,Side}
    if flags == "1307"
        sell
    elseif flags == "1303"
        buy
    end
end

"Get the side associated with a given flags integer"
function parse_dxfeed_side(flags::Integer)::Union{Nothing,Side}
    if flags == 1307
        sell
    elseif flags == 1303
        buy
    end
end

"Parse a list of events in full order depth data"
function parse_full_order_depth(path)
    CSV.File(
        path;
        missingstring = "\\NULL",
        datarow = 2,
        header = [
            "data_feed",
            "event_symbol",
            "event_time",
            "index",
            "time",
            "sequence",
            "price",
            "size",
            "flags",
            "market_maker",
            "event_flags"
        ],
        silencewarnings = true
    )
end

"Parse a set of event flags"
function parse_event_flags(flags::AbstractString)::EventFlags
    if flags == "EventFlags=SNAPSHOT_BEGIN"
        EventFlags(true, false, false, false)
    elseif flags == "EventFlags=SNAPSHOT_END"
        EventFlags(false, true, false, false)
    elseif flags == "EventFlags=TX_PENDING"
        EventFlags(false, false, true, false)
    elseif flags == "EventFlags=REMOVE_EVENT"
        EventFlags(false, false, false, true)
    else
        EventFlags(false, false, false, false)
    end
end

function parse_event_flags(flags::Missing)::EventFlags
    EventFlags(false, false, false, false)
end

"Parse the event_time flag of a dxfeed event"
function parse_dxfeed_event_time(time::AbstractString)::DateTime
    # println(time)
    DateTime(time, dateformat"yyyymmdd-HHMMSS.sss-0500")
end

function parse_dxfeed_time(time::AbstractString)::DateTime
    # println(time)
    DateTime(time, dateformat"yyyymmdd-HHMMSS-0500")
end

"Parse a dxfeed event"
function parse_dxfeed_event(row)::Event
    Event(
        row.event_symbol,
        parse_dxfeed_event_time(row.event_time),
        row.index,
        parse_dxfeed_time(row.time),
        row.sequence,
        row.price,
        row.size,
        parse_dxfeed_side(row.flags),
        row.market_maker,
        parse_event_flags(row.event_flags)
    )
end

function parse_dxfeed_event(line::AbstractString)::Union{Missing,Event}
        
    line = map(strip, split(line, ","))
    
    if length(line) != 10 && length(line) != 11
        return missing
    end
    
    market_maker = line[10]
    if market_maker == "\\NULL"
        return missing
    end
    
    price = parse(Float32, line[7])
    if price == NaN
        return missing
    end
    
    event_symbol = line[2]
    event_time = parse_dxfeed_event_time(line[3])
    index = parse(UInt64, line[4])
    time = parse_dxfeed_time(line[5])
    sequence = line[6]
    size = parse(UInt32, line[8])
    side = parse_dxfeed_side(line[9])
    event_flags = parse_event_flags(missing)
    
    if length(line) == 11
        event_flags = parse_event_flags(line[11])
    end
    
    Event(
        event_symbol,
        event_time,
        index,
        time,
        sequence,
        price,
        size,
        side,
        market_maker,
        event_flags
    )
end

"Flags for an event"
struct EventFlags
    "Whether this event marks the beginning of a snapshot"
    snapshot_begin::Bool
    "Whether this event marks the end of a snapshot"
    snapshot_end::Bool
    "Whether the transaction associated by this event is pending"
    tx_pending::Bool
    "Whether this event is to remove a transaction from the order book"
    remove_event::Bool
end

"An event in the order book data"
struct Event
    "The stock ticker associated with this event"
    stock_ticker::String
    "The time of this event" # TODO
    event_time::DateTime
    "The index of this event"
    index::UInt64
    "The time of this event" # TODO
    time::DateTime
    "The sequence of this event, to distinguish it from otherwise identical events"
    sequence::String
    "The price of the order associated with this event"
    price::Float32
    "The size of the order associated with this event"
    size::UInt32
    "Whether this order is on the buy side or sell side"
    side::Side
    "The market maker executing this order"
    market_maker::String
    "The event flags associated with this event"
    event_flags::EventFlags
end

"An order in an order book"
struct Order
    "The size of the order"
    size::Int32
    "The index of the order"
    index::Int64
end

# We can compare orders by their index, which makes things faster
function Base.isequal(left::Order, right::Order)::Bool
    left.index == right.index
end

# We also hash orders by their index for maximum performance, ignoring their size
function Base.hash(a::Order, h::UInt)
    hash(a.index, h)
end

"A consistent order book"
struct OrderBook
    stock_ticker::String # TODO: think about tagging with exchange
    buy_side::PriorityQueue{Order,Float32, Base.Order.ReverseOrdering{Base.Order.ForwardOrdering}}
    sell_side::PriorityQueue{Order, Float32, Base.Order.ForwardOrdering}
    end

function OrderBook(stock_ticker::String)
    OrderBook(
        stock_ticker,
        PriorityQueue{Order, Float32}(Base.Order.Reverse),
        PriorityQueue{Order, Float32}()
    )
end

"Make an order book from a given buy side and sell side"
function make_order_book(stock_ticker::String, buy_side::Array{Tuple{Order,Float32}}, sell_side::Array{Tuple{Order,Float32}})
    order_book = OrderBook(stock_ticker)

    for (order, price) in buy_side
        push!(order_book.buy_side, order => price)
    end

    for (order, price) in sell_side
        push!(order_book.sell_side, order => price)
    end
    
    order_book
end

"Apply a buy order to a consistent order book. Return how much money that order actally moved"
function apply_buy_order!(book::OrderBook, index::UInt64, size::UInt32, price::Float32)::Float32
    money_moved::Float32 = 0
    # Try to fulfil the order from the sell side
    while size > 0 && length(book.sell_side) != 0
        best_ask, best_ask_price = peek(book.sell_side)
        if best_ask_price <= price # We can directly fulfil some of this order
            dequeue!(book.sell_side) # Pop off the best ask
            if best_ask.size <= size # The best ask is completely fulfilled
                money_moved += best_ask.size * best_ask_price
                size -= best_ask.size
            else # The order is completely fulfilled, but some of the best ask's volume remains
                money_moved += size * best_ask_price
                new_order = Order(
                    best_ask.size - size,
                    best_ask.index
                )
                enqueue!(book.sell_side, new_order, best_ask_price)
                size = 0
            end
        else # We cannot fulfil the rest of this order
            break
        end
    end
    # If there remains a part of ther order we can't fulfil, stick it on the buy side of the order book.
    if size != 0
        enqueue!(book.buy_side, Order(size, index), price)
        money_moved += size * price
        size = 0
    end
    return money_moved
end

"Apply a sell order to a consistent order book. Return how much money that order actally moved"
function apply_sell_order!(book::OrderBook, index::UInt64, size::UInt32, price::Float32)::Float32
    money_moved::Float32 = 0
    # Try to fulfil the order from the buy side
    while size > 0 && length(book.buy_side) != 0
        best_bid, best_bid_price = peek(book.buy_side)
        if best_bid_price >= price # We can directly fulfil some of this order
            dequeue!(book.buy_side) # Pop off the best bid
            if best_bid.size <= size # The best bid is completely fulfilled
                money_moved += best_bid.size * best_bid_price
                size -= best_bid.size
            else # The order is completely fulfilled, but some of the best bid's volume remains
                money_moved += size * best_bid_price
                new_order = Order(
                    best_bid.size - size,
                    best_bid.index
                )
                enqueue!(book.buy_side, new_order, best_bid_price)
                size = 0
                break
            end
        else # We cannot fulfil the rest of this order
            break
        end
    end
    # If there remains a part of ther order we can't fulfil, stick it on the sell side of the order book.
    if size != 0
        enqueue!(book.sell_side, Order(size, index), price)
        money_moved += size * price
        size = 0
    end
    return money_moved
end

            "Apply an order to a consistent order book. Return how much money that order actually moved"
function apply_order!(book::OrderBook, index::UInt64, size::UInt32, price::Float32, side::Side)::Float32
    if side == buy
        apply_buy_order!(book, index, size, price)
    elseif side == sell
        apply_sell_order!(book, index, size, price)
    end
end

"Apply an order to a consistent order book, as described by an event"
function apply_order!(book::OrderBook, event::Event)::Float32
    apply_order!(book, event.index, event.size, event.price, event.side)
end

"An order book reconstruction algorithm"
mutable struct OrderBookReconstructor
    consistent_order_book::OrderBook
    pending_queue::Array{Event}
    is_snapshot::Bool
    tx_pending::Bool
    has_snapshot::Bool
end

function OrderBookReconstructor(stock_ticker::String)
    OrderBookReconstructor(
        OrderBook(stock_ticker),
        Event[],
        false, false, false
    )
end

"""
Handle an event, modifying the current consistent order book as necessary. 
Based off the algorithm in Section 3.6 in the [DxFeed documentation](https://downloads.dxfeed.com/specifications/dxFeed-Order-Book-Reconstruction.pdf),
with steps commented below
"""
function handle_event!(bookmaker::OrderBookReconstructor, event::Event)
# Step 1
    if event.event_flags.snapshot_begin
        empty!(bookmaker.pending_queue)
        bookmaker.is_snapshot = true
        bookmaker.has_snapshot = false
    end
        
    # Step 2
    if event.event_flags.snapshot_end && bookmaker.is_snapshot
        bookmaker.is_snapshot = false
        bookmaker.has_snapshot = true
    end
   
    # Step 3 + Step 4
    bookmaker.tx_pending = event.event_flags.tx_pending
    
    # Step 5
    push!(bookmaker.pending_queue, event)

    # Step 6
    if !bookmaker.is_snapshot && !bookmaker.tx_pending
        # Step 6.a
        if bookmaker.has_snapshot
            # TODO: think of a better way to clear the order book...
            bookmaker.consistent_order_book = OrderBook(bookmaker.consistent_order_book.stock_ticker)
            bookmaker.has_snapshot = false
        end
        # Step 6.b
        for event in bookmaker.pending_queue
        if event.event_flags.remove_event # TODO
                throw("Event flag REMOVE_EVENT not yet implemented!")
            else # TODO: *update* event? *should* be workable due to index-based priority...
                apply_order!(bookmaker.consistent_order_book, event)
            end
        end
        # Step 6.c
        empty!(bookmaker.pending_queue)
    end
end

end # module
