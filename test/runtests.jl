using Chumonsho
using Test

@testset "Parsing tests" begin
    using Chumonsho: parse_dxfeed_side, sell, buy, parse_dxfeed_event, Event, EventFlags, parse_dxfeed_time, parse_dxfeed_event_time
    @test parse_dxfeed_side("1307") == sell
    @test parse_dxfeed_side("1303") == buy
    @test parse_dxfeed_side("bad") === nothing
    @test parse_dxfeed_side(1307) == sell
    @test parse_dxfeed_side(1303) == buy
    @test parse_dxfeed_side(5432) === nothing
    sample_event = "Order#NTV,AAPL,20190807-065959.999-0500,242,20190807-070021-0500,744:0,194.6,40,1303,NSDQ,EventFlags=SNAPSHOT_BEGIN"
    sample_parse = Event(
        "AAPL",
        parse_dxfeed_event_time("20190807-065959.999-0500"),
        242,
        parse_dxfeed_time("20190807-070021-0500"),
        "744:0",
        194.6,
        40,
        buy,
        "NSDQ",
        EventFlags(true, false, false, false)
    )
    @test parse_dxfeed_event(sample_event) == sample_parse
end

@testset "Basic order book test" begin
    using Base.Iterators: zip
    using Chumonsho: Order, OrderBook, OrderBookReconstructor, parse_dxfeed_event, handle_event!, make_order_book
    events = [
        "Order#FAKE,FAKE,20200101-000001.000-0500,1,20200101-000002-0500,1,4,200,1303,FAKE",
        "Order#FAKE,FAKE,20200101-000003.000-0500,2,20200101-000004-0500,1,4.3,191,1303,FAKE",
        "Order#FAKE,FAKE,20200101-000005.000-0500,3,20200101-000006-0500,1,4.2,100,1307,FAKE",
        "Order#FAKE,FAKE,20200101-000007.000-0500,4,20200101-000008-0500,1,4.25,300,1303,FAKE",
        "Order#FAKE,FAKE,20200101-000009.000-0500,5,20200101-000010-0500,1,4.28,600,1307,FAKE",
        "Order#FAKE,FAKE,20200101-000011.000-0500,6,20200101-000012-0500,1,4.21,200,1303,FAKE",
        "Order#FAKE,FAKE,20200101-000013.000-0500,7,20200101-000014-0500,1,4.19,200,1307,FAKE",
        "Order#FAKE,FAKE,20200101-000015.000-0500,8,20200101-000016-0500,1,4.2,200,1307,FAKE",
        "Order#FAKE,FAKE,20200101-000017.000-0500,9,20200101-000018-0500,1,4.31,300,1307,FAKE",
        "Order#FAKE,FAKE,20200101-000019.000-0500,10,20200101-000020-0500,1,4.32,300,1303,FAKE"
    ]
    order_book_states::Array{Tuple{Array{Tuple{Order, Float32}}, Array{Tuple{Order, Float32}}}} = [
        ([(Order(200, 1), 4)], []),
        ([(Order(200, 1), 4), (Order(191, 2), 4.3)], []),
        ([(Order(200, 1), 4), (Order(91, 2), 4.3)], []),
        ([(Order(200, 1), 4), (Order(300, 4), 4.25), (Order(91, 2), 4.3)], []),
        ([(Order(200, 1), 4), (Order(300, 4), 4.25)], [(Order(509, 5), 4.28)]),
        ([(Order(200, 1), 4), (Order(200, 6), 4.21), (Order(300, 4), 4.25)], [(Order(509, 5), 4.28)]),
        ([(Order(200, 1), 4), (Order(200, 6), 4.21), (Order(100, 4), 4.25)], [(Order(509, 5), 4.28)]),
        ([(Order(200, 1), 4), (Order(100, 6), 4.21)], [(Order(509, 5), 4.28)]),
        ([(Order(200, 1), 4), (Order(100, 6), 4.21)], [(Order(509, 5), 4.28), (Order(300, 9), 4.31)]),
        ([(Order(200, 1), 4), (Order(100, 6), 4.21)], [(Order(209, 5), 4.28), (Order(300, 9), 4.31)])
    ]
    bookmaker = OrderBookReconstructor("FAKE")
    ix = 0
    for (event, (buy, sell)) in zip(events, order_book_states)
        handle_event!(bookmaker, parse_dxfeed_event(event))
        state = make_order_book("FAKE", buy, sell)
        @test bookmaker.consistent_order_book.stock_ticker == state.stock_ticker
        @test bookmaker.consistent_order_book.sell_side == state.sell_side
        @test bookmaker.consistent_order_book.buy_side == state.buy_side
    end
end
